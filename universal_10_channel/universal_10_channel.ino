unsigned long interval=10;//teensy transmit rate 100 Hz
unsigned long previousMs=0;
unsigned long currentMs;
//Serial is USB, Serial1 is BlueTooth

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial1.begin(115200);//boud rate, match with the receiver side
}

void loop() {
  // put your main code here, to run repeatedly:
  currentMs=millis();
  
  int val;
  if(currentMs-previousMs>interval)
  {
    previousMs=currentMs;
    for(int pin=23;pin>13;--pin)
    {
      val=analogRead(pin);
      val=map(val,0,1023,1000,2023);

      Serial.print(val);
      Serial1.print(val);
      if (pin>14)
      {
        Serial.print(',');
        Serial1.print(',');
      }

    }
    Serial.print('!');
    Serial1.print('!');
  }
}
