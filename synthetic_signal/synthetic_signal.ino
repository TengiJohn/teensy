unsigned long interval=10;//teensy transmit rate 100 Hz
unsigned long previousMs=0;
unsigned long count=0;
float phase = 0.0;
float twopi = 3.1415926 * 2;
int fs=4;
unsigned long currentMs;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);//boud rate, match with the receiver side
  Serial1.begin(115200);//boud rate, match with the receiver 
}

void loop() {
  // put your main code here, to run repeatedly:

  currentMs=millis();
  
  int val;
  if(currentMs-previousMs>interval)
  { 
    previousMs=currentMs;
    for(int pin=23;pin>13;--pin)
    {
      if(pin==14)
      {
        val = sin(phase*fs) * 2000.0 + 2050.0;
        phase = phase + twopi/(1000/interval);
        if (phase >= twopi) phase = 0;

      } 
      else
      {
        val=analogRead(pin);
      }
      val=map(val,0,1023,1000,2023);

      Serial.print(val);
      Serial1.print(val);
      if (pin>14)
      {
        Serial.print(',');
        Serial1.print(',');
        //msg=msg+val+',';
      }

    }
    Serial.print('!');
    Serial1.print('!');
    //Serial.print(msg);
  }
}
